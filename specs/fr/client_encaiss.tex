\section{Spécification des clients d'encaissement}

Les clients d'encaissement, comme leur nom l'indique, permettent d'enregistrer des paiements et générer des Tickets. Ils constituent les logiciels principaux à l'utilisation de Pastèque sur le point de vente.

Différents clients peuvent cohabiter mais un seul est nécessaire pour faire fonctionner la suite Pastèque.

\subsection{Configuration}

Un client d'enregistrement est associé à un serveur et une Caisse. La configuration commune à tous les clients d'encaissement consiste alors à renseigner l'adresse du serveur, les identifiants du compte Pastèque ainsi que le nom de la Caisse.

Cette Caisse doit être renseignée coté serveur via un client de gestion. Par défaut une Caisse est pré-définie sous le nom "Caisse".

D'autres paramètres peuvent s'ajouter au client pour la connexion à une imprimante par exemple.

\subsection{Procédure d'encaissement}

L'encaissement consiste à créer un Ticket à partir d'une Commande et envoyer le résultat au Serveur Pastèque.

\subsubsection{D'une Commande à un Ticket}

L'encaissement d'un Ticket est réalisé de manière atomique. Lorsque qu'une Commande est encaissée, son montant total doit être couvert par un ou plusieurs paiements par mode identique ou différent. Par exemple une commande de 10 euros peut être réglée avec un ticket restaurant de 3~euros, un ticket restaurant de 5~euros et 2~euros en espèces (il y a alors 3~paiements différents sur le Ticket).

Pour encaisser une Commande en plusieurs fois, celle-ci doit être découpée en plusieurs Tickets. La Commande initiale est découpée en plusieurs Commandes afin de les régler séparément. Chaque groupe d'encaissement à un instant T doit donner lieu à la création d'un Ticket. La procédure de saisie des paiements doit donc être réalisée en une fois, c'est à dire ne conserver les informations des paiements individuels de la Commande que le temps de la saisie des autres paiements de cette Commande. En cas d'annulation, toutes les opérations de paiements ne sont pas enregistrés et la Commande reste en l'état.

% Sécurisation des paiements pendant la procédure de paiement : contrer la méthode saisie totale -> annuler -> suppression de la Commande.

Lorsque la Commande est réglée en totalité, celle-ci est détruite au profit du Ticket associé. La transformation d'une Commande en Ticket est définitive. Pour toute correction, de nouvelles Commandes sont réalisées puis transformées en Tickets avec par exemple le contenu de la commande originale et des paiements à valeurs négatives et positives pour corriger le mode d'encaissement, ou encore des lignes de produits à quantités négatives et positives pour corriger le contenu de la commande originale. Dans le cas de Tickets correctifs, une référence au Ticket initial peut être indiquée.

\subsubsection{Enregistrement sur le Serveur Pastèque}

La création d'un Ticket et l'établissement d'un Z peuvent être effectués de manière synchrone ou asynchrone. Dans le cas d'un enregistrement synchrone, les enregistrements sont gérés directement coté serveur, il suffit alors de s'assurer que l'enregistrement a bel et bien eu lieu via l'analyse de la réponse du Serveur Pastèque.

Dans les faits l'enregistrement asynchrone est utilisé quasi-systématiquement. L'enregistrement asynchrone permet de conserver les données en local et les reverser à un instant T. Ce découpage permet notamment de palier à des coupures de connexions Internet ou même l'utilisation sans connexion (sur un marché par exemple) ainsi que d'obtenir la réactivité nécessaire pour effectuer un grand nombre d'encaissement rapidement.

Dans le cas d'enregistrement asynchrone, les données conservées en local sont sujettes à l'implémentation de sécurités afin de s'assurer de l'inaltérabilité des données avant le reversement sur le Serveur Pastèque. Ces données font l'objet des mêmes contraintes que les API critiques. Avant envoi, l'intégrité des données locales est vérifiée. En cas d'erreur, celle-ci doit être indiquée au Serveur Pastèque lors du reversement des données.

L'enregistrement asynchrone présente un certain risque~: celui de la perte de données en cas de défaillance matérielle en cours de Session. Ce mode doit donc être utilisé de manière éclairée. Dans le cadre d'usage normal, reverser les données tant qu'une connexion au Serveur Pastèque est disponible et par exception laisser le choix des reversements à l'utilisateur lorsque la connexion est trop dégradée (cas d'utilisation en extérieur et en mobilité par exemple).

Ainsi lorsque le cas se présente, la perte de données peut être justifiée et éventuellement corrigée par l'utilisateur à posteriori afin d'attester de sa bonne foi. Par exemple avec un rapprochement des stocks, contrôle du fond de caisse ou tout autre moyen.

\subsection{Gestion de Session}

Les encaissements sont réalisés dans le cadre d'une Session de caisse. Une Session permet de regrouper l'ensemble des Tickets pour la création d'un Z.

Une Session est rattachée à une Caisse. Elle est ouverte à un instant T et clôturée à un instant T'. L'intervalle de temps entre l'ouverture et la fermeture est laissé au choix de l'utilisateur. Chaque Session est numérotée pour la Caisse, cette séquence peut être calculée localement en cas d'ouverture asynchrone ou indiquée par le Serveur Pastèque lors de la récupération des informations de la Caisse. En cas d'usage par exemple sur un festival hors connexion sur plusieurs jours, plusieurs Sessions peuvent être réalisées hors-connexion. Lors de la première connexion d'un Client Pastèque au Serveur, les informations de Caisse et le numéro de Session sont obligatoirement récupérées depuis le Serveur Pastèque.

La clôture d'une Session donne lieu à l'édition d'un Z. Le Z récapitule la somme des montants hors taxe, des montants de TVA collectées par taux, la somme des montants TTC et le total de chacun des modes de paiements.

L'utilisation de plusieurs Clients Pastèque sur la même Caisse est une faute d'utilisation. Lorsque le cas est détecté, une alerte doit être présentée à l'utilisateur mais ne doit pas être bloquante. En effet un problème technique pourrait bloquer définitivement une Caisse. Par exemple une tablette tactile qui serait défaillante en cours de Session ne doit pas interdire l'utilisation d'une tablette de remplacement si disponible pour terminer la Session.
