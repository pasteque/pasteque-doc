# Pasteque Documentation

## API specification

The API specification is written in the OpenAPI format and is available in api.yaml.

It can be read with [SwaggerUI](https://swagger.io/tools/swagger-ui/), [SwaggerEditor](https://swagger.io/tools/swagger-editor/) or generated with offline tools. [ReDoc](https://github.com/Redocly/redoc) was selected for static generation. It makes use of some custom vendor extensions and the result may be a little different from SwaggerUI.

### Generate the static API documentation file

#### Prerequisites

Install NodeJS and install the redoc module with npm. From this directory:

```
npm install redoc-cli
```

This will install all the required modules into the directory named `node_module`.

#### Generate the file

Generate the static html file with the following command:

```
node node_modules/.bin/redoc-cli bundle api.yaml --options.hideHostname --options.menuToggle --options.nativeScrollbars --options.hideDownloadButton --disableGoogleFont
```

This will generate the file `redoc-static.html` that is readable by any web browser.
