# Changelog
All notable changes to this project will be documented in this file.


## [API 8r4] 2021-06-01

## Extensions
- Add error responses when sending invalid data
- Add scaleType 3 (time)


## [API 8r3] 2020-12-14

### New endpoinds
- GET /api/option/getAll
- GET /api/option/{name}
- DELETE /api/option/{name}
- POST /api/option
- POST /api/tax

### Extensions
- GET /api/sync and GET /api/sync/{cashRegister} now include options.

### Corrections
GET /api/paymentmodes/getAll => GET /api/paymentmode/getAll (without 's')


## [API 8r2] 2020-05-19

### New endpoints
- PATCH /api/cashregister/{reference}
- GET /api/category/getChildren/{reference}
- PATCH /api/resource/{reference}
- PATCH /api/tariffarea/{reference}

### Extensions
- Revision in /api/version.
- csPerpetual in cash sessions.
- dateStart and dateStop in GET /api/fisca/export
- count in GET /api/ticket/search.
- offset, limit and count in GET /api/ticket/session/{cashregister}/{sequence}
- 400 response when trying to remove the 'main' flag of a currency.
- 400 response when getting/setting an image with an invalid model name.
- 404 response for PATCH endpoints with error body when the record was not found.
- 404 response for GET endpoints when requesting records related to an other one and the later is not found.
- Error body in response 404 for some GET endpoints that didn't return null in revision 1.
- Error body in response 400 when reference is not unique in POST, PUT, PATCH endpoints.
- Error body in response 400 when a referenced record is not found in POST, PUT or PATCH endpoints.
- Error body in POST /api/cashsession for responses 400.
- Description message for response 400 when setting an id in PUT or PATCH endpoints.

### Deprecated endpoints
- GET /api/category/getChildren
- POST /api/cashregister
- POST /api/category
- POST /api/currency
- POST /api/paymentmode
- POST /api/product
- POST /api/resource
- POST /api/tariffarea

### Misc
- GET /api/cashsession/{cashregisterid}/{sequence} does not imply conformance with the LF2016


## [API 8r1-2] 2020-05-04

### Corrections
- PaymentMode.returns.paymentMode => PaymentMode.returns.returnMode.
- DELETE /api/resource/{reference} => reference is string, return integer.
- DELETE /api/tariffarea/{reference} => no response 400.
